import { SignInForm, BannerSignIn } from '../components/organisms';

const SignIn = () => {
  return (
    <section className='sign-in mx-auto'>
      <div className='row'>
        <div className='col-xxl-5 col-lg-6 my-auto py-lg-0 pt-lg-50 pb-lg-50 pt-30 pb-47 px-0'>
          <form action=''>
            <SignInForm />
          </form>
        </div>
        <BannerSignIn />
      </div>
    </section>
  );
};

export default SignIn;
