import React from 'react';
import { OverviewContent, Sidebar } from '../../components/organisms';

const Member = () => {
  return (
    <section className='overview overflow-auto'>
      <Sidebar activeMenu='overview' />
      <OverviewContent />
    </section>
  );
};

export default Member;
