import {
  Sidebar,
  TransactionDetailContent,
} from '../../../components/organisms';

const TransactionsDetail = () => {
  return (
    <>
      <Sidebar activeMenu={'transactions'} />
      <TransactionDetailContent />
    </>
  );
};

export default TransactionsDetail;
