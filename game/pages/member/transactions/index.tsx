import { Sidebar, TransactionsContent } from '../../../components/organisms';

const Transactions = () => {
  return (
    <>
      <Sidebar activeMenu='transactions' />
      <TransactionsContent />
    </>
  );
};

export default Transactions;
