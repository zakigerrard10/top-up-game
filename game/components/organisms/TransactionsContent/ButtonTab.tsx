import classNames from 'classnames';
import Link from 'next/link';

interface ButtonTabProps {
  href: string;
  active: boolean;
  title: string;
}

const ButtonTab = (props: Partial<ButtonTabProps>) => {
  const { title, href = '/', active = false } = props;
  const classTab = classNames({
    'btn btn-status rounded-pill text-sm me-3': true,
    'btn-active': active,
  });
  return (
    <Link href={href}>
      <a data-filter='*' className={classTab}>
        {title}
      </a>
    </Link>
  );
};

export default ButtonTab;
