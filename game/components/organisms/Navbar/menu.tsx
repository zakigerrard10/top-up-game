import Link from 'next/link';
import cx from 'classnames';

interface MenuProps {
  title: string;
  href?: string;
  active?: boolean;
}

const Menu = (props: Partial<MenuProps>) => {
  const { title, active = false, href = '/' } = props;
  const classTitle = cx({
    'nav-link': true,
    active,
  });
  return (
    <li className='nav-item my-auto'>
      <Link href={href}>
        <a className={classTitle} aria-current='page'>
          {title}
        </a>
      </Link>
    </li>
  );
};

export default Menu;
