import Link from 'next/link';
import React from 'react';

interface ItemListProps {
  link: string;
  title: string;
}

const ItemList = (props: Partial<ItemListProps>) => {
  const { link = '/#', title } = props;
  return (
    <li className='mb-6'>
      <Link href={link}>
        <a className='text-lg color-palette-1 text-decoration-none'>{title}</a>
      </Link>
    </li>
  );
};

export default ItemList;
