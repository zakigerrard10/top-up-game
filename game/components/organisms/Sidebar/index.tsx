import Footer from './Footer';
import MenuItem from './MenuItem';
import Profile from './Profile';

interface SidebarProps {
  activeMenu: 'overview' | 'transactions' | 'settings';
}

const Sidebar = (props: SidebarProps) => {
  const { activeMenu } = props;
  return (
    <section className='sidebar'>
      <div className='content pt-50 pb-30 ps-30'>
        <Profile />
        <div className='menus'>
          <MenuItem
            title='Overview'
            icon={'ic-overview'}
            active={activeMenu === 'overview'}
            href={'/member'}
          />
          <MenuItem
            title=' Transactions'
            icon={'ic-transactions'}
            active={activeMenu === 'transactions'}
            href={'/member/transactions'}
          />
          <MenuItem title=' Messages' icon={'ic-messages'} />
          <MenuItem title=' Card' icon={'ic-card'} />
          <MenuItem title=' Rewards' icon={'ic-rewards'} />
          <MenuItem
            title=' Settings'
            icon={'ic-settings'}
            active={activeMenu === 'settings'}
            href={'/member/edit-profile'}
          />
          <MenuItem title=' Logout' icon={'ic-logout'} />
        </div>
        <Footer />
      </div>
    </section>
  );
};

export default Sidebar;
