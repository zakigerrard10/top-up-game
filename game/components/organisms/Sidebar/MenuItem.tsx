import Image from 'next/image';
import cx from 'classnames';
import Link from 'next/link';

interface MenuItemProps {
  title: string;
  icon:
    | 'ic-overview'
    | 'ic-transactions'
    | 'ic-messages'
    | 'ic-card'
    | 'ic-rewards'
    | 'ic-settings'
    | 'ic-logout';
  active: boolean;
  href: string;
}

const MenuItem = (props: Partial<MenuItemProps>) => {
  const { title, icon, active = false, href = '/member' } = props;
  const classMenu = cx({
    item: true,
    'mb-30': true,
    active: active,
  });
  return (
    <div className={classMenu}>
      <div className='me-3'>
        <Image src={`/icon/${icon}.svg`} width={25} height={25} alt='icon' />
      </div>
      <p className='item-title m-0'>
        <Link href={href}>
          <a className='text-lg text-decoration-none'>{title}</a>
        </Link>
      </p>
    </div>
  );
};

export default MenuItem;
