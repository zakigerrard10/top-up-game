import { ReachedItem } from '../../molucules';
import Line from './Line';

const Reached = () => {
  return (
    <section className='reached pt-50 pb-50'>
      <div className='container-fluid'>
        <div className='d-flex flex-lg-row flex-column align-items-center justify-content-center gap-lg-0 gap-4'>
          <ReachedItem reach={'290M+'} title='Players Top Up' />
          <Line />
          <ReachedItem reach={'12.500'} title='Games Available' />
          <Line />
          <ReachedItem reach={'99,9%'} title='Happy Players' />
          <Line />
          <ReachedItem reach={'4.7'} title='Rating Worldwide' />
        </div>
      </div>
    </section>
  );
};

export default Reached;
